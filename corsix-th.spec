%define uri com.corsixth.%{name}
%global debug_package %{nil}

Name:			corsix-th
Version:		0.62
Release:		3%{?dist}
Summary:		Open source clone of Theme Hospital
License:		MIT
URL:			http://corsixth.com
Source0:		https://github.com/CorsixTH/CorsixTH/archive/v%{version}.tar.gz
Source1:		https://github.com/CorsixTH/CorsixTH/raw/master/CorsixTH/Original_Logo.svg
Source2:		https://gitlab.com/rmnvgr/copr-corsixth/raw/master/%{uri}.desktop
Source3:		https://gitlab.com/rmnvgr/copr-corsixth/raw/master/%{uri}.appdata.xml

BuildRequires:	cmake
BuildRequires:	gcc-c++
BuildRequires:	desktop-file-utils
BuildRequires:	libappstream-glib
BuildRequires:	inkscape
BuildRequires:	SDL2-devel >= 2.0
BuildRequires:	SDL2_mixer-devel >= 2.0
BuildRequires:	freetype-devel >= 2.5.3
BuildRequires:	lua-devel >= 5.1
Requires:		luarocks
Requires:		lua-filesystem

%description
A reimplementation of the 1997 Bullfrog business sim Theme Hospital. As well as faithfully recreating the original, CorsixTH adds support for modern operating systems (Windows, Mac OSX and Linux), high resolutions and much more.

%prep
%setup -n CorsixTH-%{version}
for x in 16 22 24 32 48 64 128 256 512
	do mkdir -p icons/${x}x${x}/apps
	inkscape -z -e icons/${x}x${x}/apps/%{uri}.png -w ${x} -h ${x} %{SOURCE1}
done

%build
%cmake -D WITH_MOVIES:BOOL=OFF -DCMAKE_INSTALL_PREFIX=%{_prefix} .
%make_build

%install
%make_install
install -D -p -m 644 LICENSE.txt %{buildroot}%{_datadir}/licenses/%{name}/LICENSE
for x in 16 22 24 32 48 64 128 256 512
	do install -D -p -m 644 icons/${x}x${x}/apps/%{uri}.png %{buildroot}%{_datadir}/icons/hicolor/${x}x${x}/apps/%{uri}.png
done
install -D -p -m 644 %{SOURCE1} %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/%{uri}.svg
desktop-file-install %{SOURCE2}
install -D -p -m 644 %{SOURCE3} %{buildroot}%{_metainfodir}/%{uri}.appdata.xml

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/%{uri}.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{uri}.appdata.xml

%files
%{_bindir}/%{name}
%{_datadir}/%{name}/*
%{_datadir}/licenses/%{name}/LICENSE
%{_datadir}/icons/hicolor/*/apps/%{uri}.png
%{_datadir}/icons/hicolor/scalable/apps/%{uri}.svg
%{_datadir}/applications/%{uri}.desktop
%{_metainfodir}/%{uri}.appdata.xml

%changelog
* Thu Aug 30 2018 - 0.62-3
- Add gcc-c++ as build dependency
- Build for Fedora 29 and Rawhide

* Thu Jul 26 2018 - 0.62-2
- Fix icon name in .desktop file

* Sat Jul 21 2018 - 0.62-1
- Package renamed corsix-th following upstream change
- SVG icon from upstream repository
- PNG icons generated from SVG
- Put .desktop in external file
- Added .appdata.xml for GUI application centers
